import _magnum
import habitat
import habitat_sim
from PIL import Image
from habitat.sims.habitat_simulator.actions import HabitatSimActions
import cv2
import envs.spring_env
import math
import warnings
import visitor_utils
import pyvista as pv
import os
import random
import numpy as np
from habitat_sim.utils.common import d3_40_colors_rgb
from cocostuffapi.PythonAPI.pycocotools.cocostuffhelper import segmentationToCocoMask
from pycocotools import mask



FORWARD_KEY="w"
LEFT_KEY="a"
RIGHT_KEY="d"
FINISH="f"
LOOK_UP_KEY="u"
LOOK_DOWN_KEY="j"


HOSPITAL_1 = "hospital_1"
HOSPITAL_2 = "hospital_2"
LIVINGLAB_1 = "livinglab_1"
LIVINGLAB_2 = "livinglab_2"
B315 = "b315"
APARTMENT = "apartment"
B670 = "b670"
CORRIDOR = "corridor"
CASTLE = "castle"
SEMANTIC_H = "semantic_h" #IN progress hospital
SEMANTIC_L = "semantic_l" #IN progress living lab
HL2COLMAP = "hl2col"
SPSG = "spsg"

def _segmentationToPoly(mask, ):
    """
    Convert segmentation from RLE to polynoms ([[x1 y1 x2 x2 y2 ...]]). Code from https://github.com/facebookresearch/Detectron/issues/100#issuecomment-362882830.

    Parameters:
        :param mask: (array) Bitmap mask
        :return segmentationPoly: (list) Segmentation converted to polynoms
    """
    contours, _ = cv2.findContours((mask).astype(np.uint8), cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    segmentationPoly = []

    for contour in contours:
        contour = contour.flatten().tolist()
        if len(contour) > 4:
            segmentationPoly.append(contour)
    return segmentationPoly


def normalize(v, tolerance=0.00001):
    mag2 = sum(n * n for n in v)
    if mag2 == 0:
        return v
    if abs(mag2 - 1.0) > tolerance:
        mag = math.sqrt(mag2)
        i = 0
        for n in v:
            v[i] = n / mag
            i += 1
    return v

def create_quat(angle,x,y,z):
    v=[0,0,0,0]
    v[0] = math.cos(math.radians(angle / 2))
    v[1] = math.sin(math.radians(angle / 2)) * math.cos(math.radians(x))
    v[2] = math.sin(math.radians(angle / 2)) * math.cos(math.radians(y))
    v[3] = math.sin(math.radians(angle / 2)) * math.cos(math.radians(z))
    return v

def transform_rgb_bgr(image):
    return image[:, :, [2, 1, 0]]


def searchfor(matrix, number):
    for i in range(len(matrix)):
        for j in range(len(matrix[i])):
            if matrix[i][j] == number:
                return i
    return 666

def bbox(segmentation):
    if (len(segmentation) == 0 or len(segmentation[0]) == 0 or len(segmentation[1]) == 0):
        return [0,0,0,0]
    else:
        miny=min(segmentation[0])
        maxy=max(segmentation[0])
        minx=min(segmentation[1])
        maxx=max(segmentation[1])
        return [float(minx),float(miny),float(maxx-minx),float(maxy-miny)]



def visit(location="not set"):

    config=habitat.get_config("configs/tasks/pointnav.yaml")
    #config = habitat.get_config("configs/tasks/pointnav_single_camera.yaml")

    objects = []
    #env = habitat.Env(config=config)
    env = envs.spring_env.SpringEnv(
        config=config,
        objects=objects,
    )

    if location == HOSPITAL_1:
        env.episodes[0].scene_id = ("data/scene_datasets/hospital_1.glb")
    elif location == HOSPITAL_2:
        env.episodes[0].scene_id = ("data/scene_datasets/hospital_2.glb")
    elif location == LIVINGLAB_1:
        env.episodes[0].scene_id = ("data/scene_datasets/livinglab_1.glb")
    elif location == LIVINGLAB_2:
        env.episodes[0].scene_id = ("data/scene_datasets/livinglab_2.glb")
    elif location == B315:
        env.episodes[0].scene_id = ("data/scene_datasets/B315.glb")
    elif location == B670:
        env.episodes[0].scene_id = ("data/scene_datasets/B670.glb")
    elif location == APARTMENT:
        env.episodes[0].scene_id = ("data/scene_datasets/apartment_1.glb")
    elif location == CORRIDOR:
        env.episodes[0].scene_id = ("data/scene_datasets/corridor.glb")
    elif location == CASTLE:
        env.episodes[0].scene_id = ("data/scene_datasets/skokloster-castle.glb")
    elif location == SEMANTIC_H:
        env.episodes[0].scene_id = ("data/scene_datasets/hospital_empty.glb") #hospital_empty.glb is missing roof
    elif location == SEMANTIC_L:
        env.episodes[0].scene_id = ("data/scene_datasets/living_lab_empty.glb")  #living_lab_empty.glb is missing roof
    elif location == HL2COLMAP:
        env.episodes[0].scene_id = ("data/scene_datasets/HL2COLMAP_noSPSG.glb")
    elif location == SPSG:
        print("SPSG LOCATION SELECTED")
        env.episodes[0].scene_id = ("data/scene_datasets/B-635_SPSG.glb")
    else:
        warnings.warn("Place not set or unrecognized, using HOSPITAL_1(default)")
        env.episodes[0].scene_id = ("data/scene_datasets/hospital_1.glb")

    print("JSON initiated")
    f = open("data/datasets/coco/labels.json", "a")
    f.write("{\n\t\"info\": {\"description\": \"BROCA hospital segmentation dataset\"},\n")
    f.write("\t\"licenses\": {\"id\": 1,\"name\": \"Attribution-NonCommercial-ShareAlike License\",\"url\": \"http://creativecommons.org/licenses/by-nc-sa/2.0/\"}, \n")
    f.write("\t\"categories\": [\n")
    f.close()

    #print(env.episodes[0].scene_id)
    print("Environment creation successful")
    observations = env.reset()

    if location ==  SEMANTIC_H:
        orig_path = "data/objects_categorized/Hospital"
    if location == SEMANTIC_L:
        orig_path = "data/objects_categorized/Living_lab"
    if location == HL2COLMAP:
        orig_path = "data/objects_categorized/HL2COLMAP"
    if location == SPSG:
        orig_path = "data/objects_categorized/SPSG"
    orig_dirs = os.listdir(orig_path)
    i = 0
    category_names = []
    category_ids = []
    for file in orig_dirs:
        if not file.startswith('.'):
            category_names.append(str(file))
            path = orig_path + "/" + file
            dirs = os.listdir(path)
            insert = []
            for item in dirs:
                if item.endswith(".glb"):
                    i += 1
                    insert.append(i)
                    final_path = path + "/" + item[:-4]
                    name_obj = path + "/" + file + "_obj/" + item[:-4] + ".obj"
                    print(name_obj)
                    obj_file=pv.read(name_obj)
                    obj_file.rotate_x(-90) #IDK IF THIS WILL BE NECESARRY BUT WE WILL SEE WHAT HABITAT DOES WITH NEW SCENES
                    coords=obj_file.center
                    env.add_object(final_path[4:], coords, i)  # Weird scaling + translation
            category_ids.append(insert)

    #JSON for categories (open file write into it)
    for i in range(len(category_ids)):
        f = open("data/datasets/coco/labels.json", "a")
        f.write("\t\t{\"id\": " + str(i+1) + ",\"name\": \"" + category_names[i] + "\",\"supercategory\": \"" + category_names[i] + "\"},\n")
        f.close()
    print(category_names)
    print(category_ids)

    f = open("data/datasets/coco/labels_copy.json", "a")
    f.write("\t\"annotations\": [\n")  # Not sure about this
    f.close()

    f = open("data/datasets/coco/labels.json", "a")
    f.write("\t],\n\t\"images\": [\n")  # Not sure about this
    f.close()
    cnt = 0 #5229
    #Sample dataset INSERT DESIRED NUMBER OF IMAGES PLZZZ

    print("SIMULATION STARTING")
    for i in range (200): #Image id is i+1


        #Find random point in map
        new_point = env.sim.sample_navigable_point()
        #print("NEW SAMPLED POINT IS:")
        #print(new_point)

        #Add random angles + heights
        height_off=random.uniform(-1.3,1.1)
        x_axis_off=random.uniform(-5,5)
        z_axis_off=random.uniform(0,360)

        #Apply randomness
        while new_point[1]>3:
            new_point = env.sim.sample_navigable_point()
        height_check = new_point[1] + height_off
        if (height_check > 0.7):
            y_axis_off = random.uniform(-25, 0)
        else:
            y_axis_off = random.uniform(0, 25)
        rotation=create_quat(180,0+x_axis_off,90+y_axis_off ,0+z_axis_off) #x= 0 +- 5  y= 90 +- 20 z = angle
        #BACHA JESTLI JE TENHLE QUATERNION SPRÁVNĚ!!!!
        #print(rotation)

        observations = env.sim.get_observations_at(new_point,normalize(rotation)) #get observations at random point
        rgb = transform_rgb_bgr(observations["rgb"]) #create RGB scan

        #Visualize for detecting object and categorize what is seen
        semantic_img =Image.new("P", (observations['semantic'].shape[1], observations['semantic'].shape[0]))
        semantic_img.putpalette(d3_40_colors_rgb.flatten())
        semantic_img.putdata((observations['semantic'].flatten()).astype(np.uint8))
        semantic_img = np.array(semantic_img)

        obj_ids = [x for x in np.unique(semantic_img)]

        #print(obj_ids)

        #print(semantic_img)
        #print(len(semantic_img))
        #print(len(semantic_img[0]))

        #dimensions=semantic_img.shape
        #print(dimensions)

        #Create JSON FOR image (open file, write new image...)

        f = open("data/datasets/coco/labels.json", "a")
        f.write("\t\t{\"id\": " + str(i+1) + ",\"license\": 1,\"file_name\": \"" + str(i+1) + ".jpg\","
                "\"height\": 480, \"width\": 640, \"date_captured\": \"2021-09-09 13:00:00\"},\n")
        f.close()

        #JSON
        for j in range(1,len(obj_ids)):
            cnt += 1
            obj_id=obj_ids[j]
            #print(obj_id)
            result=np.where(semantic_img==obj_id)
            #print(result)
            #print(len(result))
            #print(len(result[0]))
            #print(len(result[1]))
            #print(len(semantic_img[0]))
            """
            segmentation=[]
            for k in range(len(result[1])):
                if (len(result[1])==len(result[0])):
                    segmentation.append(result[1][k])
                    segmentation.append(result[0][k])
                else:
                    print("SOMETHING IS REALLY WRONG")
            #segmentation=np.array(segmentation)
            """
            #area = len(result[0]) # ==mask.area(seg)
            iscrowd = 0
            image_id=i+1    #ID of image
            category_id=searchfor(category_ids,obj_id) #Name of category
            b_box = bbox(result) #B_box in format [x,y,width,height]
            seg = segmentationToCocoMask(semantic_img, obj_id) #Segmentaton from semantic img
            area = float(mask.area(seg))  #Area of segmentation
            #b_box = mask.toBbox(seg).flatten().tolist() # == bbox(result)
            seg['counts'] = str(seg['counts'], "utf-8")
            bitmap = mask.decode(seg)
            seg = _segmentationToPoly(bitmap)

            #print(segmentation)

            #b_box = mask.toBbox(seg).flatten().tolist()
            #print(b_box)
            #print(category_id)
            #print(category_names[category_id])

            #print(category_id+1)

            #print(int(b_box[0]), int(b_box[1]))
            #cv2.line(semantic_img, (int(b_box[0]), int(b_box[1])), (int(b_box[0]), int(b_box[3])), (255, 255, 255), 3)
            #cv2.line(semantic_img, (int(b_box[2]), int(b_box[1])), (int(b_box[2]), int(b_box[3])), (255, 255, 255), 3)
            #cv2.line(semantic_img, (int(b_box[0]), int(b_box[1])), (int(b_box[2]), int(b_box[1])), (255, 255, 255), 3)
            #cv2.line(semantic_img, (int(b_box[0]), int(b_box[3])), (int(b_box[2]), int(b_box[3])), (255, 255, 255), 3)

            #Annotations JSON, napsat file a pak zkopírovat

            if (result != [] and result[0] != [] and result[1] != [] and seg != [] ):
                f = open("data/datasets/coco/labels_copy.json", "a")
                f.write("\t\t{\n\t\t\t\"id\": " + str(cnt) + ",\n")#Not sure about this
                f.write("\t\t\t\"image_id\": " + str(i+1) + ",\n")
                f.write("\t\t\t\"category_id\": " + str(category_id+1) + ",\n")
                f.write("\t\t\t\"bbox\": " + str(b_box) + ",\n")
                f.write("\t\t\t\"segmentation\": " + str(seg) + ",\n")
                f.write("\t\t\t\"area\": " + str(area) + ",\n")
                f.write("\t\t\t\"iscrowd\": " + str(iscrowd) + "\n")
                f.write("\t\t},\n")
                f.close()
                print("JSON annotation added")
            else:
                cnt -= 1





        #print(result)

        #Visualization with colours

        semantic_img = Image.new("P", (observations['semantic'].shape[1], observations['semantic'].shape[0]))
        semantic_img.putpalette(d3_40_colors_rgb.flatten())
        semantic_img.putdata((observations['semantic'].flatten() % 40).astype(np.uint8))
        semantic_img = semantic_img.convert("RGB")
        semantic_img = np.array(semantic_img)

        #cv2.circle(semantic_img, (1, 1), 2, (255, 255, 255), -1)
        #cv2.imshow("RGB",rgb)
        #cv2.imshow("semantic", semantic_img)

        name=str(i+1)+".jpg"
        path_rgb = "data/datasets/coco/rgbs/" + name
        cv2.imwrite(path_rgb, rgb)
        #cv2.waitKey(0)

        print("\n\n\n NEW IMAGE no:" + str(i+1) + "\n\n\n")

    f = open("data/datasets/coco/labels.json", "a")
    f.write("\t],\n}")  # Not sure about this
    f.close()

    f = open("data/datasets/coco/labels_copy.json", "a")
    f.write("\t]")  # Not sure about this
    f.close

    #img_dir = os.path.join(os.getcwd(), 'data/datasets/coco/images')
    #ann_dir = os.path.join(os.getcwd(), 'data/datasets/coco/annotations')
    #cas = COCO_Assistant(img_dir, ann_dir)

    #Stepping in enviroment
    """
    print("Destination, distance: {:3f}, theta(radians): {:.2f}".format(
        observations["pointgoal_with_gps_compass"][0],
        observations["pointgoal_with_gps_compass"][1]))
    """
    print("Sampling completed!")
    """
    print("Agent stepping around inside environment.")

    count_steps = 0
    while not env.episode_over:
        keystroke = cv2.waitKey(0)

        if keystroke == ord(FORWARD_KEY):
            action = HabitatSimActions.MOVE_FORWARD
            print("action: FORWARD")
        elif keystroke == ord(LEFT_KEY):
            action = HabitatSimActions.TURN_LEFT
            print("action: LEFT")
        elif keystroke == ord(RIGHT_KEY):
            action = HabitatSimActions.TURN_RIGHT
            print("action: RIGHT")
        elif keystroke == ord(FINISH):
            action = HabitatSimActions.STOP
            print("action: FINISH")
        elif keystroke == ord(LOOK_UP_KEY):
            action = HabitatSimActions.LOOK_UP
            print("action: LOOK_UP")
        elif keystroke == ord(LOOK_DOWN_KEY):
            action = HabitatSimActions.LOOK_DOWN
            print("action: LOOK_DOWN")
        else:
            print("INVALID KEY")
            continue

        observations = env.step(action)
        count_steps += 1

        print("Destination, distance: {:3f}, theta(radians): {:.2f}".format(
            observations["pointgoal_with_gps_compass"][0],
            observations["pointgoal_with_gps_compass"][1]))
        visitor_utils.show_observations(observations)
    

    print("Episode finished after {} steps.".format(count_steps))

    if (
        action == HabitatSimActions.STOP
        and observations["pointgoal_with_gps_compass"][0] < 0.2
    ):
        print("you successfully navigated to destination point")
    else:
        print("your navigation was unsuccessful")
    """

if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument('-l', "--location",
                        action='store',
                        help='The location to visit. Known locations: hospital_1, hospital_2, livinglab_1, livinglab_2, default(hospital_1).',
                        dest="location")
    parser.set_defaults(location=HOSPITAL_1)
    args = parser.parse_args()
    visit(args.location)
