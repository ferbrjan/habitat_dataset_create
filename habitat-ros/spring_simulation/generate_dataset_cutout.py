import _magnum
import habitat
import habitat_sim
from PIL import Image
from habitat.sims.habitat_simulator.actions import HabitatSimActions
import cv2
import envs.spring_env
import math
import warnings
import visitor_utils
import pyvista as pv
import os
import random
import numpy as np
from habitat_sim.utils.common import d3_40_colors_rgb
from cocostuffapi.PythonAPI.pycocotools.cocostuffhelper import segmentationToCocoMask
from pycocotools import mask
from scipy.spatial.transform import Rotation as R
import csv



FORWARD_KEY="w"
LEFT_KEY="a"
RIGHT_KEY="d"
FINISH="f"
LOOK_UP_KEY="u"
LOOK_DOWN_KEY="j"


HOSPITAL_1 = "hospital_1"
HOSPITAL_2 = "hospital_2"
LIVINGLAB_1 = "livinglab_1"
LIVINGLAB_2 = "livinglab_2"
B315 = "b315"
APARTMENT = "apartment"
B670 = "b670"
CORRIDOR = "corridor"
CASTLE = "castle"
SEMANTIC_H = "semantic_h" #IN progress hospital
SEMANTIC_L = "semantic_l" #IN progress living lab

def _segmentationToPoly(mask, ):
    contours, _ = cv2.findContours((mask).astype(np.uint8), cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    segmentationPoly = []

    for contour in contours:
        contour = contour.flatten().tolist()
        if len(contour) > 4:
            segmentationPoly.append(contour)
    return segmentationPoly


def normalize(v, tolerance=0.00001):
    mag2 = sum(n * n for n in v)
    if mag2 == 0:
        return v
    if abs(mag2 - 1.0) > tolerance:
        mag = math.sqrt(mag2)
        i = 0
        for n in v:
            v[i] = n / mag
            i += 1
    return v

def quaternion_multiply(quaternion1, quaternion0):
    w0, x0, y0, z0 = quaternion0
    w1, x1, y1, z1 = quaternion1
    return np.array([-x1 * x0 - y1 * y0 - z1 * z0 + w1 * w0,
                     x1 * w0 + y1 * z0 - z1 * y0 + w1 * x0,
                     -x1 * z0 + y1 * w0 + z1 * x0 + w1 * y0,
                     x1 * y0 - y1 * x0 + z1 * w0 + w1 * z0], dtype=np.float64)

def quaternion_magic(quaternion): #Habitat stupid formating
    w, x, y, z = quaternion
    return [x,y,z,w]

def create_quat(angle, x, y, z):
    v = [0, 0, 0, 0]
    v[0] = math.cos(math.radians(angle / 2))
    v[1] = math.sin(math.radians(angle / 2)) * math.cos(math.radians(x))
    v[2] = math.sin(math.radians(angle / 2)) * math.cos(math.radians(y))
    v[3] = math.sin(math.radians(angle / 2)) * math.cos(math.radians(z))
    return v

def transform_rgb_bgr(image):
    return image[:, :, [2, 1, 0]]


def searchfor(matrix, number):
    for i in range(len(matrix)):
        for j in range(len(matrix[i])):
            if matrix[i][j] == number:
                return i
    return 666

def bbox(segmentation):
    if (len(segmentation) == 0 or len(segmentation[0]) == 0 or len(segmentation[1]) == 0):
        return [0,0,0,0]
    else:
        miny=min(segmentation[0])
        maxy=max(segmentation[0])
        minx=min(segmentation[1])
        maxx=max(segmentation[1])
        return [float(minx),float(miny),float(maxx-minx),float(maxy-miny)]

def rotate_point(point): #x rot -90
    return Rx(-90).dot(point)

def Rz(angle):
    angle = np.radians(angle)
    return np.array([[np.cos(angle), -np.sin(angle),0],
                     [np.sin(angle), np.cos(angle),0],
                     [0, 0, 1]])

def Rx(angle):
    angle = np.radians(angle)
    return np.array([[1, 0, 0],
                     [0, np.cos(angle), -np.sin(angle)],
                     [0, np.sin(angle), np.cos(angle)]])

def Ry(angle):
    angle = np.radians(angle)
    return np.array([[np.cos(angle), 0, np.sin(angle)],
                     [0, 1, 0],
                     [-np.sin(angle), 0, np.cos(angle)]])

def quat2rotm(quat):
    return 0



def visit(location="not set"):

    config=habitat.get_config("configs/tasks/pointnav.yaml")
    config.defrost()
    config.SIMULATOR.DEPTH_SENSOR.NORMALIZE_DEPTH = False
    config.freeze()
    #config = habitat.get_config("configs/tasks/pointnav_single_camera.yaml")

    objects = []
    #env = habitat.Env(config=config)
    env = envs.spring_env.SpringEnv(
        config=config,
        objects=objects,
    )

    if location == HOSPITAL_1:
        env.episodes[0].scene_id = ("data/scene_datasets/hospital_1.glb")
    elif location == HOSPITAL_2:
        env.episodes[0].scene_id = ("data/scene_datasets/hospital_2.glb")
    elif location == LIVINGLAB_1:
        env.episodes[0].scene_id = ("data/scene_datasets/livinglab_1.glb")
    elif location == LIVINGLAB_2:
        env.episodes[0].scene_id = ("data/scene_datasets/livinglab_2.glb")
    elif location == B315:
        env.episodes[0].scene_id = ("data/scene_datasets/B315.glb")
    elif location == B670:
        env.episodes[0].scene_id = ("data/scene_datasets/B670.glb")
    elif location == APARTMENT:
        env.episodes[0].scene_id = ("data/scene_datasets/apartment_1.glb")
    elif location == CORRIDOR:
        env.episodes[0].scene_id = ("data/scene_datasets/corridor.glb")
    elif location == CASTLE:
        env.episodes[0].scene_id = ("data/scene_datasets/skokloster-castle.glb")
    elif location == SEMANTIC_H:
        env.episodes[0].scene_id = ("data/scene_datasets/hospital_empty.glb") #hospital_empty.glb is missing roof
    elif location == SEMANTIC_L:
        env.episodes[0].scene_id = ("data/scene_datasets/living_lab_empty.glb")  #living_lab_empty.glb is missing roof
    else:
        warnings.warn("Place not set or unrecognized, using HOSPITAL_1(default)")
        env.episodes[0].scene_id = ("data/scene_datasets/hospital_1.glb")

    print("JSON initiated")
    f = open("data/datasets/cutout_dataset/labels.json", "a")
    f.write("{\n\t\"info\": {\"description\": \"BROCA hospital segmentation dataset\"},\n")
    f.write("\t\"licenses\": {\"id\": 1,\"name\": \"Attribution-NonCommercial-ShareAlike License\",\"url\": \"http://creativecommons.org/licenses/by-nc-sa/2.0/\"}, \n")
    f.write("\t\"categories\": [\n")
    f.close()

    #print(env.episodes[0].scene_id)
    print("Environment creation successful")
    observations = env.reset()

    if location ==  SEMANTIC_H:
        orig_path = "data/objects_categorized/Hospital"
    if location == SEMANTIC_L:
        orig_path = "data/objects_categorized/Living_lab"
    orig_dirs = os.listdir(orig_path)
    i = 0
    category_names = []
    category_ids = []
    for file in orig_dirs:
        if not file.startswith('.'):
            category_names.append(str(file))
            path = orig_path + "/" + file
            dirs = os.listdir(path)
            insert = []
            for item in dirs:
                if item.endswith(".glb"):
                    i += 1
                    insert.append(i)
                    final_path = path + "/" + item[:-4]
                    name_obj = path + "/" + file + "_obj/" + item[:-4] + ".obj"
                    obj_file=pv.read(name_obj)
                    obj_file.rotate_x(-90)
                    coords=obj_file.center
                    env.add_object(final_path[4:], coords, i)  # Weird scaling + translation
            category_ids.append(insert)

    #JSON for categories (open file write into it)
    for i in range(len(category_ids)):
        f = open("data/datasets/cutout_dataset/labels.json", "a")
        f.write("\t\t{\"id\": " + str(i+1) + ",\"name\": \"" + category_names[i] + "\",\"supercategory\": \"" + category_names[i] + "\"},\n")
        f.close()
    #print(category_names)
    #print(category_ids)

    f = open("data/datasets/cutout_dataset/labels_copy.json", "a")
    f.write("\t\"annotations\": [\n")
    f.close()

    f = open("data/datasets/cutout_dataset/labels.json", "a")
    f.write("\t],\n\t\"images\": [\n")
    f.close()
    cnt = 0

    #INITIALIZE READING CSV FILE
    csv_file = open("data/datasets/cutout_dataset/poses.csv",newline='')
    reader = csv.reader(csv_file)
    cnt_id = 0

    for row in reader: #for each line in text file
        cnt_id += 1

        #Read txt file with quat + pos
        cutout_name = row[0]
        point = np.array(row[1:4])
        point = point.astype(np.float)
        quat = np.array([row[5], row[6], row[7], row[4]])
        quat = quat.astype(np.float)

        point_x_rot = rotate_point(point)
        quat = R.from_quat(quat)
        quat_h2m = R.from_matrix(Rx(-90))
        quat_my2 = (quat_h2m * quat * R.from_matrix(Rx(180))).as_quat()


        observations = env.sim.get_observations_at(point_x_rot, quat_my2)

        visitor_utils.show_observations(observations)
        cv2.waitKey(0)

        # RGB img for saving
        rgb_img = transform_rgb_bgr(observations["rgb"])  # create RGB scan

        # Semantic img for saving
        semantic_img = Image.new("P", (observations['semantic'].shape[1], observations['semantic'].shape[0]))
        semantic_img.putpalette(d3_40_colors_rgb.flatten())
        semantic_img.putdata((observations['semantic'].flatten()).astype(np.uint8))
        semantic_img = np.array(semantic_img)

        depth_img = observations["depth"]

        obj_ids = [x for x in np.unique(semantic_img)]

        # EDIT FOR SPECIFIC CUTOUT
        f = open("data/datasets/cutout_dataset/labels.json", "a")

        f.write("\t\t{\"id\": " + str(cnt_id) + ",\"license\": 1,\"file_name\": \"" + cutout_name + "\","
                "\"height\": 756, \"width\": 1344, \"date_captured\": \"2021-09-09 13:00:00\"},\n")
        f.close()
        # EDIT FOR SPECIFIC CUTOUT

        #JSON
        for j in range(1,len(obj_ids)):
            cnt += 1
            obj_id=obj_ids[j]
            result=np.where(semantic_img==obj_id)
            iscrowd = 0
            category_id=searchfor(category_ids,obj_id) #Name of category
            b_box = bbox(result) #B_box in format [x,y,width,height]
            seg = segmentationToCocoMask(semantic_img, obj_id) #Segmentaton from semantic img
            area = float(mask.area(seg))  #Area of segmentation
            seg['counts'] = str(seg['counts'], "utf-8")
            bitmap = mask.decode(seg)
            seg = _segmentationToPoly(bitmap)

            #Annotations JSON, napsat file a pak zkopírovat

            if (result != [] and result[0] != [] and result[1] != [] and seg != [] ):
                f = open("data/datasets/cutout_dataset/labels_copy.json", "a")
                f.write("\t\t{\n\t\t\t\"id\": " + str(cnt) + ",\n")#Not sure about this
                f.write("\t\t\t\"image_id\": " + str(cnt_id) + ",\n")
                f.write("\t\t\t\"category_id\": " + str(category_id+1) + ",\n")
                f.write("\t\t\t\"bbox\": " + str(b_box) + ",\n")
                f.write("\t\t\t\"segmentation\": " + str(seg) + ",\n")
                f.write("\t\t\t\"area\": " + str(area) + ",\n")
                f.write("\t\t\t\"iscrowd\": " + str(iscrowd) + "\n")
                f.write("\t\t},\n")
                f.close()
                #print("JSON annotation added")
            else:
                cnt -= 1

        name=cutout_name
        path_rgb = "data/datasets/cutout_dataset/rgbs/" + name #CHANGE TO DESIRED RGB img NAME
        cv2.imwrite(path_rgb, rgb_img)
        path_semantic = "data/datasets/cutout_dataset/semantic/" + name  #CHANGE TO DESIRED semantic img NAME
        cv2.imwrite(path_semantic, semantic_img)
        path_depth = "data/datasets/cutout_dataset/depth/" + name
        cv2.imwrite(path_depth, depth_img)
        #cv2.waitKey(0)

        print("\n\n\n NEW IMAGE no:" + str(cnt_id) + "\n\n\n")

    csv_file.close()

    f = open("data/datasets/cutout_dataset/labels.json", "a")
    f.write("\t],\n}")  # Not sure about this
    f.close()

    f = open("data/datasets/cutout_dataset/labels_copy.json", "a")
    f.write("\t]")  # Not sure about this
    f.close
    print("Sampling completed!")
if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument('-l', "--location",
                        action='store',
                        help='The location to visit. Known locations: hospital_1, hospital_2, livinglab_1, livinglab_2, default(hospital_1).',
                        dest="location")
    parser.set_defaults(location=HOSPITAL_1)
    args = parser.parse_args()
    visit(args.location)
