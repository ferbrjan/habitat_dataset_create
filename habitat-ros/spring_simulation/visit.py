import habitat
from habitat.sims.habitat_simulator.actions import HabitatSimActions
import cv2
import envs.spring_env
import warnings
import visitor_utils
from PIL import Image
import numpy as np
from habitat_sim.utils.common import d3_40_colors_rgb
import math


def transform_rgb_bgr(image):
    return image[:, :, [2, 1, 0]]

def quaternion_multiply(quaternion1, quaternion0):
    w0, x0, y0, z0 = quaternion0
    w1, x1, y1, z1 = quaternion1
    return np.array([-x1 * x0 - y1 * y0 - z1 * z0 + w1 * w0,
                     x1 * w0 + y1 * z0 - z1 * y0 + w1 * x0,
                     -x1 * z0 + y1 * w0 + z1 * x0 + w1 * y0,
                     x1 * y0 - y1 * x0 + z1 * w0 + w1 * z0], dtype=np.float64)

def quaternion_magic(quaternion): #Habitat stupid formating
    w, x, y, z = quaternion
    return [x,y,z,w]

def create_quat(angle, x, y, z):
    v = [0, 0, 0, 0]
    v[0] = math.cos(math.radians(angle / 2))
    v[1] = math.sin(math.radians(angle / 2)) * math.cos(math.radians(x))
    v[2] = math.sin(math.radians(angle / 2)) * math.cos(math.radians(y))
    v[3] = math.sin(math.radians(angle / 2)) * math.cos(math.radians(z))
    return v

def rotate_point(point): #x rot -90
    x,y,z = point
    return [x,z,-y]


FORWARD_KEY="w"
LEFT_KEY="a"
RIGHT_KEY="d"
FINISH="f"
LOOK_UP_KEY="u"
LOOK_DOWN_KEY="j"


HOSPITAL_1 = "hospital_1"
HOSPITAL_2 = "hospital_2"
LIVINGLAB_1 = "livinglab_1"
LIVINGLAB_2 = "livinglab_2"
B315 = "b315"
APARTMENT = "apartment"
B670 = "b670"
CORRIDOR = "corridor"
CASTLE = "castle"


def visit(location="not set"):

    config=habitat.get_config("configs/tasks/pointnav.yaml")
    #config = habitat.get_config("configs/tasks/pointnav_single_camera.yaml")

    objects = []
    #env = habitat.Env(config=config)
    env = envs.spring_env.SpringEnv(
        config=config,
        objects=objects,
    )

    if location == HOSPITAL_1:
        env.episodes[0].scene_id = ("data/scene_datasets/HL2COLMAP_noSPSG.glb")
    elif location == HOSPITAL_2:
        env.episodes[0].scene_id = ("data/scene_datasets/hospital_2.glb")
    elif location == LIVINGLAB_1:
        env.episodes[0].scene_id = ("data/scene_datasets/livinglab_1.glb")
    elif location == LIVINGLAB_2:
        env.episodes[0].scene_id = ("data/scene_datasets/livinglab_2.glb")
    elif location == B315:
        env.episodes[0].scene_id = ("data/scene_datasets/B315.glb")
    elif location == B670:
        env.episodes[0].scene_id = ("data/scene_datasets/B670.glb")
    elif location == APARTMENT:
        env.episodes[0].scene_id = ("data/scene_datasets/apartment_1.glb")
    elif location == CORRIDOR:
        env.episodes[0].scene_id = ("data/scene_datasets/corridor.glb")
    elif location == CASTLE:
        env.episodes[0].scene_id = ("data/scene_datasets/skokloster-castle.glb")
    else:
        warnings.warn("Place not set or unrecognized, using HL2COLMAP(default)")
        env.episodes[0].scene_id = ("data/scene_datasets/HL2COLMAP_noSPSG.glb")

    print("Environment creation successful")
    observations = env.reset()
    print("Destination, distance: {:3f}, theta(radians): {:.2f}".format(
        observations["pointgoal_with_gps_compass"][0],
        observations["pointgoal_with_gps_compass"][1]))

    """
    for i in range (10000):

        point = [9.491846, -3.260349, 1.595966]
        quat = [0.685412, 0.687064, 0.175787, -0.165084] # pi/2

        #MATH HERE!!!!!
        point_x_rot = rotate_point(point)  # OK
        quat_my = quaternion_multiply(quat,create_quat(-90,0,90,90))
        #quat = create_quat(0,90,90,90)

        observations = env.sim.get_observations_at(point_x_rot, quaternion_magic(quat))
        visitor_utils.show_observations(observations)

        # RGB img for saving
        rgb = transform_rgb_bgr(observations["rgb"])  # create RGB scan

        # Semantic img for saving
        semantic_img = Image.new("P", (observations['semantic'].shape[1], observations['semantic'].shape[0]))
        semantic_img.putpalette(d3_40_colors_rgb.flatten())
        semantic_img.putdata((observations['semantic'].flatten()).astype(np.uint8))
        semantic_img = np.array(semantic_img)
    


    cv2.waitKey(0)
    """
    new_point = env.sim.sample_navigable_point()
    observations = env.sim.get_observations_at(new_point)
    visitor_utils.show_observations(observations)
    cv2.waitKey(0)
    new_point = env.sim.sample_navigable_point()
    observations = env.sim.get_observations_at(new_point)
    visitor_utils.show_observations(observations)
    cv2.waitKey(0)
    new_point = env.sim.sample_navigable_point()
    observations = env.sim.get_observations_at(new_point)
    visitor_utils.show_observations(observations)
    cv2.waitKey(0)
    print("Agent stepping around inside environment.")

    count_steps = 0
    while not env.episode_over:
        keystroke = cv2.waitKey(0)

        if keystroke == ord(FORWARD_KEY):
            action = HabitatSimActions.MOVE_FORWARD
            print("action: FORWARD")
        elif keystroke == ord(LEFT_KEY):
            action = HabitatSimActions.TURN_LEFT
            print("action: LEFT")
        elif keystroke == ord(RIGHT_KEY):
            action = HabitatSimActions.TURN_RIGHT
            print("action: RIGHT")
        elif keystroke == ord(FINISH):
            action = HabitatSimActions.STOP
            print("action: FINISH")
        elif keystroke == ord(LOOK_UP_KEY):
            action = HabitatSimActions.LOOK_UP
            print("action: LOOK_UP")
        elif keystroke == ord(LOOK_DOWN_KEY):
            action = HabitatSimActions.LOOK_DOWN
            print("action: LOOK_DOWN")
        else:
            print("INVALID KEY")
            continue

        observations = env.step(action)
        count_steps += 1
        state=env.sim.get_agent_state()
        print("STATE OF AGENT IS:")
        print(state)

        print("Destination, distance: {:3f}, theta(radians): {:.2f}".format(
            observations["pointgoal_with_gps_compass"][0],
            observations["pointgoal_with_gps_compass"][1]))
        visitor_utils.show_observations(observations)


    print("Episode finished after {} steps.".format(count_steps))

    if (
        action == HabitatSimActions.STOP
        and observations["pointgoal_with_gps_compass"][0] < 0.2
    ):
        print("you successfully navigated to destination point")
    else:
        print("your navigation was unsuccessful")


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument('-l', "--location",
                        action='store',
                        help='The location to visit. Known locations: hospital_1, hospital_2, livinglab_1, livinglab_2, default(hospital_1).',
                        dest="location")
    parser.set_defaults(location=HOSPITAL_1)
    args = parser.parse_args()
    visit(args.location)
