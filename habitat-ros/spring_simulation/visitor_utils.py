import cv2
import numpy as np
from PIL import Image
from habitat_sim.utils.common import d3_40_colors_rgb


def transform_rgb_bgr(image):
    return image[:, :, [2, 1, 0]]


def show_observations2(observations):
    numpy_horizontal1 = np.hstack((transform_rgb_bgr(observations["rgb"]), transform_rgb_bgr(observations["rgb1"])))

    semantic_img = Image.new("P", (observations['semantic'].shape[1], observations['semantic'].shape[0]))
    semantic_img.putpalette(d3_40_colors_rgb.flatten())
    semantic_img.putdata((observations['semantic'].flatten() % 40).astype(np.uint8))
    semantic_img = semantic_img.convert("RGB")
    semantic_img = np.array(semantic_img)
    #print(semantic_img.shape)
    #print(observations['depth'].shape)
    depth_image = np.stack((observations['depth'], observations['depth'], observations['depth']), axis=2)[:, :, :, 0]
    depth_image = (255*depth_image).astype(np.uint8)
    #print(depth_image.shape)
    numpy_horizontal2 = np.hstack((semantic_img, depth_image))
    numpy_vertical = np.vstack((numpy_horizontal1, numpy_horizontal2))
    cv2.imshow("RGB+RGB1+SEMANTIC+DEPTH", numpy_vertical)
    #print(observations["agent_state"])




def show_observations(observations):
    numpy_horizontal1 = np.hstack((transform_rgb_bgr(observations["rgb"]), transform_rgb_bgr(observations["rgb1"])))
    semantic_img = Image.new("P", (observations['semantic'].shape[1], observations['semantic'].shape[0]))
    semantic_img.putpalette(d3_40_colors_rgb.flatten())
    semantic_img.putdata((observations['semantic'].flatten() % 40).astype(np.uint8))
    semantic_img = semantic_img.convert("RGB")
    semantic_img = np.array(semantic_img)
    #print(semantic_img.shape)
    #print(observations['depth'].shape)
    depth_image = np.stack((observations['depth'], observations['depth'], observations['depth']), axis=2)[:, :, :, 0]
    depth_image = (255*depth_image).astype(np.uint8)
    #print(depth_image.shape)
    numpy_horizontal2 = np.hstack((semantic_img, depth_image))
    numpy_vertical = np.vstack((numpy_horizontal1, numpy_horizontal2))
    cv2.imshow("RGB+RGB1+SEMANTIC+DEPTH", numpy_vertical)
    #print(observations["agent_state"])




